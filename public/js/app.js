var webapp = angular.module('webapp', ['ui.router']);

   webapp.config(function($stateProvider, $urlRouterProvider) {

       $urlRouterProvider.otherwise('/register');

       $stateProvider

       // HOME STATES AND NESTED VIEWS ========================================
           .state('register', {
               url: '/register',
               templateUrl: 'templates/register.html',
               controller:'registerController'
           })

           .state('employee', {
              url: '/employee',
              templateUrl: 'templates/employee.html',
              controller:'employeeController'
           })

           .state('employeeedit', {
             url: '/employeeedit/:employeeId',
             templateUrl: 'templates/employeeedit.html',
             controller:'employeeEditController'
           })

           .state('employeedetails', {
              url: '/employeedetails',
              templateUrl: 'templates/employeedetails.html',
              controller:'employeedetailcontroller'
           })

           // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
           .state('login', {
               url: '/login',
               templateUrl: 'templates/login.html',
               controller:'loginController'

               // we'll get to this in a bit
           });

   });