webapp.factory('userServices',function($http){
   var postAllRegisterDetails = function(data)
        {
           return $http.post('/userRegistration', data);
        };

   var postAllLoginDetails = function(data)
        {
            console.log("testing:::::", data)
           return $http.post('/login', data);
        };
   var postEmployeedetails=function(data)
           {
              return $http.post('/employeeDetails',data);
           }
  var getAllEmployeeDetails = function()
       {
           return $http.get('/allEmployeeDetails');
       }
  var deleteEmployee=function(employeeId)
        {
           return $http.delete('/employeeBymongoId/'+employeeId);
        }
  var getSingleEmployeeDetail=function(employeeId)
             {
                return $http.get('/singleemployeeBymongoId/'+employeeId);
             }
  var updateEmployeeDetails=function(editdata)
           {
              return $http.post('/editEmployeeBymongoId',editdata);
           }

   return   {
             postAllRegisterDetails: postAllRegisterDetails,
             postAllLoginDetails: postAllLoginDetails,
             postEmployeedetails: postEmployeedetails,
             getAllEmployeeDetails: getAllEmployeeDetails,
             deleteEmployee: deleteEmployee,
             getSingleEmployeeDetail: getSingleEmployeeDetail,
             updateEmployeeDetails: updateEmployeeDetails
            }

   });
